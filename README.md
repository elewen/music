# music

> 个人专属音乐播放器

## 项目描述
> 基于vue开发，并使用vuex进行全局状态管理。播放器内核基于浏览器原生<audio>标签实现。实现了切歌，循环，拖动播放，歌词滚动，全局小播放器模式等功能。
> 项目中所有数据来源基于qq音乐接口，由于qq音乐的官方安全策略限制，部分接口使用了axios进行代理设置。
> 此外，项目使用了路由懒加载和图片懒加载技术，提升了首屏渲染的速度，避免了不必要的资源加载。

## 版本参数
- vue ^2.5.2
- vue-router ^3.0.1
- vuex ^3.0.1
- node v9.8.0
- npm v5.6.0
- webpack ^3.6.0


## UI预览
![1](./UI/1.png)
![2](./UI/2.png)
![3](./UI/3.png)
![4](./UI/4.png)
![5](./UI/5.png)
![6](./UI/6.png)
![7](./UI/7.png)
![8](./UI/8.png)
![9](./UI/9.png)
![10](./UI/10.png)
![11](./UI/11.png)
![12](./UI/12.png)
![13](./UI/13.png)
![14](./UI/14.png)
![15](./UI/15.png)
![16](./UI/16.png)

## 怎么安装

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run a production server
node prod.server.js
```
