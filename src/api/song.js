import jsonp from 'common/js/jsonp'
import { commonParams } from './config'

export function getSong (song) {
  const url = 'https://c.y.qq.com/base/fcgi-bin/fcg_music_express_mobile3.fcg'

  const data = Object.assign({}, commonParams, {
    hostUin: 0,
    needNewCode: 0,
    platform: 'yqq',
    inCharset: 'utf8',
    loginUin: 0,
    format: 'json',
    cid: 205361747,
    uin: 0,
    guid: 1400133164,
    songmid: song.mid,
    filename: `C400${song.mid}.m4a`
  })

  return jsonp(url, data)
}
